/* file: memcached.hpp
 * author: Alexander Merritt <merritt.alex@gatech.edu>
 *
 * Integration with memcached.
 */

#ifndef MEMCACHED_HPP_INCLUDED
#define MEMCACHED_HPP_INCLUDED

#include "types.hpp"

int watch_memcached(void);

#endif /* MEMCACHED_HPP_INCLUDED */
